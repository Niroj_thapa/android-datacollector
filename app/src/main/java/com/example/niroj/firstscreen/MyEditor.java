package com.example.niroj.firstscreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

/**
 * Created by niroj on 8/11/16.
 */
public class MyEditor extends AppCompatActivity {

    EditText textIn,txtHeading;
    Button buttonAdd,btnsave;
    TextView textViewOut;
    LinearLayout container;
    DbHelper dbHelper;
    List<String> nameList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editor);
        nameList = new ArrayList<>();


        dbHelper = new DbHelper(this);
        dbHelper.getWritableDatabase();

        textViewOut = (TextView)findViewById(R.id.textout);
        textIn = (EditText)findViewById(R.id.textin);
        txtHeading = (EditText)findViewById(R.id.heading);
            buttonAdd = (Button)findViewById(R.id.add);


            container = (LinearLayout)findViewById(R.id.container);
        String extras = getIntent().getStringExtra("item");
        switch (extras){

            case "radiobutton":

                buttonAdd.setVisibility(View.VISIBLE);
                textIn.setHint("Place your Radio Button Items one by one");

                break;

            case "checkbox":

                buttonAdd.setVisibility(View.VISIBLE);
                textIn.setHint("Place your Check Box Items");


                break;

            case "textbox":

                textIn.setHint("Add your Text");


                break;



            case "plaintext":

                textIn.setHint("Add your plainText");



            default:
                break;




        }

            buttonAdd.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View arg0) {


                        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addView = layoutInflater.inflate(R.layout.list_view, null);
                        LinearLayout linearLayout=(LinearLayout)findViewById(R.id.layout_icon_select);



                            String extras = getIntent().getStringExtra("item");

                        switch (extras){

                            case "radiobutton":
                                RadioButton rb = (RadioButton)addView.findViewById(R.id.radiobutton1);
                                rb.setVisibility(View.VISIBLE);
                                rb.setEnabled(false);
                                break;

                            case "checkbox":
                                CheckBox cb = (CheckBox)addView.findViewById(R.id.checkBox1);
                                cb.setVisibility(View.VISIBLE);
                                cb.setEnabled(false);
                                break;


                            case "plaintext":




                            default:
                                break;




                        }


                        TextView textOut = (TextView)addView.findViewById(R.id.textout);

                        textOut.setText(textIn.getText().toString());


                        Button buttonRemove = (Button)addView.findViewById(R.id.remove);
                        buttonRemove.setOnClickListener(new View.OnClickListener(){

                        @Override
                        public void onClick(View v) {
                            ((LinearLayout)addView.getParent()).removeView(addView);
                        }});

                    container.addView(addView);
                }});

        btnsave =(Button) findViewById(R.id.btn_save);
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String newName = txtHeading.getText().toString().trim();
                nameList.add(newName);
                Intent intent = new Intent(getApplicationContext(),BuilderPage.class);
                intent.putStringArrayListExtra("key", (ArrayList<String>) nameList);
                startActivity(intent);
                Toast.makeText(MyEditor
                        .this,"You added" +newName.toUpperCase()+ "in your view",Toast.LENGTH_LONG).show();





                    int listsize = nameList.size();
                    for (int i = 0; i < listsize; i++) {
                        Log.i("Lists are", nameList.get(i));


                    }


                    startActivity(intent);
                    Toast.makeText(MyEditor
                            .this, "You added" + newName.toUpperCase() + "in your view", Toast.LENGTH_LONG).show();
                }


        });
    }

}

