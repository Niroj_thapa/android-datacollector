package com.example.niroj.firstscreen;

import android.animation.Animator;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.SlidingDrawer;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by niroj on 8/10/16.
 */
public class BuilderPage extends ActionBarActivity implements RecyclerViewAdapter.OnItemClickListener {
    private RecyclerView myRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerViewAdapter myRecyclerViewAdapter;



    @BindView(R.id.edittxtsurvey)
    EditText editTxtsurveyname;

    @BindView(R.id.imgviewCamera)
    ImageView camera;
    @BindView(R.id.imgviewdateAndTime)
    ImageView dateandtime;


    @BindView(R.id.ImageCameraView)
    ImageView cameraImageView;

    @BindView(R.id.slideHandleButton)
    ImageView slideHandleButton;

    @BindView(R.id.imgviewcheckbox)
    ImageView checkbox;

    @BindView(R.id.imgviewradiobutton)
    ImageView radiobutton;

    @BindView(R.id.imgviewtextbox)
    ImageView textbox;

    @BindView(R.id.imgviewplaintext)
    ImageView plaintext;

    @BindView(R.id.SlidingDrawer)
    SlidingDrawer slidingDrawer;

    @BindView(R.id.toolbar_builderxml)
    Toolbar toolbar1;

    @BindDrawable(R.drawable.arrowdown)
    protected Drawable arrowdown;

    @BindDrawable(R.drawable.arrowdown)
    protected Drawable arroowup;

    DbHelper dbHelper;
    private static final int CAMERA_REQUEST = 1888;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.builder_layout);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar1);
        getSupportActionBar().setDisplayShowTitleEnabled(false);



        dbHelper = new DbHelper(this);
        dbHelper.getWritableDatabase();



        ArrayList<String> nameList = getIntent().getStringArrayListExtra("key");
        myRecyclerView = (RecyclerView)findViewById(R.id.recyclerView_builderxml);

        linearLayoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);


        myRecyclerViewAdapter = new RecyclerViewAdapter(this,nameList);
        myRecyclerView.setAdapter(myRecyclerViewAdapter);
        myRecyclerView.setLayoutManager(linearLayoutManager);
        myRecyclerViewAdapter.setOnItemClickListener(this);





        slidingDrawer.open();



        slidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {

            @Override
            public void onDrawerClosed() {
                slideHandleButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_up_drop_circle_black_48dp));


            }
        });
        slidingDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {

            @Override
            public void onDrawerOpened() {
                slideHandleButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_down_drop_circle_black_48dp));



            }
        });
    }

    @OnClick(R.id.imgviewCamera)
    public void OnClick_Camera(View View)
    {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }


protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST) {
        Bitmap photo = (Bitmap) data.getExtras().get("data");
            cameraImageView.setImageBitmap(photo);

        }
    }

    @OnClick(R.id.imgviewdateAndTime)
    public void OnClick_DateAndTime(View view)
    {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(BuilderPage.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                selectedmonth = selectedmonth + 1;

                /*eReminderDate.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);*/
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select Date");
        mDatePicker.show();


    }

    @OnClick(R.id.imgviewtextbox)
    public void OnClick_TextBox(View view)
    {
        Intent intent = new Intent(getApplicationContext(),MyEditor.class);
        intent.putExtra("item","textbox");
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.imgviewplaintext)
    public void OnClick_PlainText(View view)
    {
        Intent intent = new Intent(getApplicationContext(),MyEditor.class);
        intent.putExtra("item","plaintext");
        startActivity(intent);
        finish();
    }


    @OnClick(R.id.imgviewcheckbox)
    public void OnClick_CheckBox(View view)
    {
        dbHelper.Insert_RadioButton("CheckBox");

        Intent intent = new Intent(getApplicationContext(),MyEditor.class);
        intent.putExtra("item","checkbox");
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.imgviewradiobutton)
    public void OnClick_RadioButton(View view)

    {
        dbHelper.Insert_RadioButton("RadioButton");
        Intent intent = new Intent(getApplicationContext(),MyEditor.class);
        intent.putExtra("item","radiobutton");
        startActivity(intent);
        finish();



    }



    // display menu incons from menu_main XML
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.builderscreen, menu);
        return true;




    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.msave){


            if (editTxtsurveyname.getText().toString().isEmpty()==true )
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(BuilderPage.this);
                builder.setMessage("Please fill the survey name")
                        .setTitle("Fix ERROR")
                        .setPositiveButton(android.R.string.ok,null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else{

                dbHelper.insertSurvey_Infro(editTxtsurveyname.getText().toString(),"Tbl_" + editTxtsurveyname.getText().toString());

                Toast.makeText(BuilderPage.this,"Survey added",Toast.LENGTH_LONG).show();
            }

        }

        return true;
    }

    @Override
    public void onItemClick(RecyclerViewAdapter.ItemHolder item, int position) {
        Toast.makeText(this,
                "Remove " + position + " : " + item.getItemName(),
                Toast.LENGTH_SHORT).show();
    }
}