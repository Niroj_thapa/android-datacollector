// Generated code from Butter Knife. Do not modify!
package com.example.niroj.firstscreen;

import android.content.Context;
import android.content.res.Resources;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.Object;
import java.lang.Override;

public final class BuilderPage_ViewBinder implements ViewBinder<BuilderPage> {
  @Override
  public Unbinder bind(Finder finder, BuilderPage target, Object source) {
    Context context = finder.getContext(source);
    Resources res = context.getResources();
    Resources.Theme theme = context.getTheme();
    return new BuilderPage_ViewBinding<>(target, finder, source, res, theme);
  }
}
