// Generated code from Butter Knife. Do not modify!
package com.example.niroj.firstscreen;

import android.content.res.Resources;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SlidingDrawer;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class BuilderPage_ViewBinding<T extends BuilderPage> implements Unbinder {
  protected T target;

  private View view2131492987;

  private View view2131492988;

  private View view2131492984;

  private View view2131492986;

  private View view2131492985;

  private View view2131492983;

  public BuilderPage_ViewBinding(final T target, Finder finder, Object source, Resources res, Resources.Theme theme) {
    this.target = target;

    View view;
    target.editTxtsurveyname = finder.findRequiredViewAsType(source, R.id.edittxtsurvey, "field 'editTxtsurveyname'", EditText.class);
    view = finder.findRequiredView(source, R.id.imgviewCamera, "field 'camera' and method 'OnClick_Camera'");
    target.camera = finder.castView(view, R.id.imgviewCamera, "field 'camera'", ImageView.class);
    view2131492987 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClick_Camera(p0);
      }
    });
    view = finder.findRequiredView(source, R.id.imgviewdateAndTime, "field 'dateandtime' and method 'OnClick_DateAndTime'");
    target.dateandtime = finder.castView(view, R.id.imgviewdateAndTime, "field 'dateandtime'", ImageView.class);
    view2131492988 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClick_DateAndTime(p0);
      }
    });
    target.cameraImageView = finder.findRequiredViewAsType(source, R.id.ImageCameraView, "field 'cameraImageView'", ImageView.class);
    target.slideHandleButton = finder.findRequiredViewAsType(source, R.id.slideHandleButton, "field 'slideHandleButton'", ImageView.class);
    view = finder.findRequiredView(source, R.id.imgviewcheckbox, "field 'checkbox' and method 'OnClick_CheckBox'");
    target.checkbox = finder.castView(view, R.id.imgviewcheckbox, "field 'checkbox'", ImageView.class);
    view2131492984 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClick_CheckBox(p0);
      }
    });
    view = finder.findRequiredView(source, R.id.imgviewradiobutton, "field 'radiobutton' and method 'OnClick_RadioButton'");
    target.radiobutton = finder.castView(view, R.id.imgviewradiobutton, "field 'radiobutton'", ImageView.class);
    view2131492986 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClick_RadioButton(p0);
      }
    });
    view = finder.findRequiredView(source, R.id.imgviewtextbox, "field 'textbox' and method 'OnClick_TextBox'");
    target.textbox = finder.castView(view, R.id.imgviewtextbox, "field 'textbox'", ImageView.class);
    view2131492985 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClick_TextBox(p0);
      }
    });
    view = finder.findRequiredView(source, R.id.imgviewplaintext, "field 'plaintext' and method 'OnClick_PlainText'");
    target.plaintext = finder.castView(view, R.id.imgviewplaintext, "field 'plaintext'", ImageView.class);
    view2131492983 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClick_PlainText(p0);
      }
    });
    target.slidingDrawer = finder.findRequiredViewAsType(source, R.id.SlidingDrawer, "field 'slidingDrawer'", SlidingDrawer.class);
    target.toolbar1 = finder.findRequiredViewAsType(source, R.id.toolbar_builderxml, "field 'toolbar1'", Toolbar.class);

    target.arrowdown = Utils.getDrawable(res, theme, R.drawable.arrowdown);
    target.arroowup = Utils.getDrawable(res, theme, R.drawable.arrowdown);
  }

  @Override
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.editTxtsurveyname = null;
    target.camera = null;
    target.dateandtime = null;
    target.cameraImageView = null;
    target.slideHandleButton = null;
    target.checkbox = null;
    target.radiobutton = null;
    target.textbox = null;
    target.plaintext = null;
    target.slidingDrawer = null;
    target.toolbar1 = null;

    view2131492987.setOnClickListener(null);
    view2131492987 = null;
    view2131492988.setOnClickListener(null);
    view2131492988 = null;
    view2131492984.setOnClickListener(null);
    view2131492984 = null;
    view2131492986.setOnClickListener(null);
    view2131492986 = null;
    view2131492985.setOnClickListener(null);
    view2131492985 = null;
    view2131492983.setOnClickListener(null);
    view2131492983 = null;

    this.target = null;
  }
}
