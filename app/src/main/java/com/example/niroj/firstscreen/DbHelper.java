package com.example.niroj.firstscreen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by niroj on 8/12/16.
 */
public class DbHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "datacollector.db";

    private static final String Table_Survey = "SURVEY";
    private static final String Survey_KEY_ID = "Survey";
    private static final String Survey_KEY_NAME = "Survey_Name";
    private static final String KEY_Table_Name = "Table_Name";


    private static final String Table_formDetails = "FormDetails";
    private static final String formDetails_KEY_ID = "Detail_id";
    private static final String formDetails_field_Type = "Field_Type";
    private static final String formDetails_label = "Label";
    private static final String formDetails_Options = "Options";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_SURVEY_TABLE = "CREATE TABLE " + Table_Survey + "("
                + Survey_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Survey_KEY_NAME + " VARCHAR(30),"
                + KEY_Table_Name + " VARCHAR(50)" + ")";
        db.execSQL(CREATE_SURVEY_TABLE);


        String CREATE_FORM_DETAILS_TABLE = "CREATE TABLE " + Table_formDetails + "("
                + formDetails_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + formDetails_field_Type + " VARCHAR(30),"
                + formDetails_Options + " VARCHAR(30),"
                + formDetails_label + " VARCHAR(30),"
                + Survey_KEY_ID + " INTEGER,"
                + " FOREIGN KEY (" + Survey_KEY_ID + ") REFERENCES " + Table_Survey + "(" + Survey_KEY_ID + "));";
        db.execSQL(CREATE_FORM_DETAILS_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        db.execSQL("DROP TABLE IF EXISTS " + Table_Survey);

        db.execSQL("DROP TABLE IF EXISTS " + Table_formDetails);
        // Create tables again
        onCreate(db);

    }

    public boolean insertSurvey_Infro(String SurveyName, String TableSurveyName) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("Survey_Name", SurveyName);
        values.put("Table_Name", TableSurveyName);

        sqLiteDatabase.insert("SURVEY", null, values);

        return true;
    }

    public boolean Insert_into_Options(String OptionsValue){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("Options",OptionsValue);

        sqLiteDatabase.insert("FormDetails",null,values);
        return true;
    }


    public boolean Insert_RadioButton(String Radio){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("Field_Type",Radio);


        sqLiteDatabase.insert("FormDetails",null,values);
        return true;

    }

    public boolean Insert_CheckBOx(String[] CheckBox){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        for(int i = 0;i<CheckBox.length;i++) {


            ContentValues values = new ContentValues();

            values.put("Field_Type", CheckBox[i]);
            sqLiteDatabase.insert("FormDetails",null,values);

        }

        return true;

    }

    public boolean Insert_FormDetails(String FieldTYpe,String Label){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("Field_Type",FieldTYpe);

        values.put("Label",Label);


        sqLiteDatabase.insert("FormDetails",null,values);
        return true;
    }

}



