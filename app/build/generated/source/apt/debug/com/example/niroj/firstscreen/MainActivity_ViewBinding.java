// Generated code from Butter Knife. Do not modify!
package com.example.niroj.firstscreen;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class MainActivity_ViewBinding<T extends MainActivity> implements Unbinder {
  protected T target;

  private View view2131492974;

  public MainActivity_ViewBinding(final T target, Finder finder, Object source) {
    this.target = target;

    View view;
    view = finder.findRequiredView(source, R.id.float_Button_MainXml, "field 'floatingActionButton' and method 'newForm'");
    target.floatingActionButton = finder.castView(view, R.id.float_Button_MainXml, "field 'floatingActionButton'", FloatingActionButton.class);
    view2131492974 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.newForm(p0);
      }
    });
    target.toolbar = finder.findRequiredViewAsType(source, R.id.toolbar_mainxml, "field 'toolbar'", Toolbar.class);
  }

  @Override
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.floatingActionButton = null;
    target.toolbar = null;

    view2131492974.setOnClickListener(null);
    view2131492974 = null;

    this.target = null;
  }
}
